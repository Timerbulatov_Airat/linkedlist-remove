package ru.sber.jd;


// метод remove ()

import java.io.*;
import java.util.LinkedList;

public class Collection {
    public static void main(String args[]) {

        //Создание пустого LinkedList
        LinkedList<String> list = new LinkedList<String>();

        // Использую метод add () для добавления элементов в список
        list.add("менеджер по обслуживанию");
        list.add("менеджер по продажам");
        list.add("сервис-менеджер");
        list.add("консультант");
        list.add("клиентский менеджер");

        // Вывожу список
        System.out.println("список должностей до ребрендинга:" + list);

        // Удаляю с  помощью remove ()
        list.remove("сервис-менеджер");
        list.remove("консультант");


        // Распечатываю окончательный список должностей после ребрендинга
        System.out.println("список должностей после ребрендинга:" + list);
    }
}